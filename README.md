# Laravel_DynamoDB
Plugin for creating DynamoDB "migrations" for Laravel
## Installation
`composer require helium/laravel-dynamodb`
## Usage
### Configuration
Add the following line to your package service providers in `app.php`

`Helium\DynamoDb\DynamoDbServiceProvider::class,`

Add the following lines to your database connections in `database.php`
```
'dynamodb' => [
    'group' => env('APP_NAME'),
    'region' => env('DYNAMO_REGION'),
    'version' => env('DYNAMO_VERSION', 'latest'),
    //'endpoint' => env('DYNAMO_HOST'),
    'credentials' => [
        'key' => env('DYNAMO_KEY'),
        'secret' => env('DYNAMO_SECRET'),
    ]
],
```
You can uncomment the `endpoint` key/value pair if you are using an endpoint other than the AWS default.

Optionally, set your default connection to `dynamodb`.

### Migration
Create a migration

`php artisan make:migration`

Replace `use Illuminate\Database\Schema\Blueprint;` with `use Helium\DynamoDb\Schema\Blueprint;`

If you did not set `dynamodb` as your default connection, specify the correct connection

`Schema::connection('dynamodb')->...`

#### Supported schema functions

- `create($table, $callback)` - Create new table
- `drop($table)` - Drop table
- `dropIfExists($table)` - Alias for `drop`

#### Supported blueprint functions

- `string($name)` - Create a new attribute with type `S`
- `number($name)` - Create a new attribute with type `N`
- `binary($name)` - Create a new attribute with type `B`
- `partitionKey($name)` - Set the table partition key using a previously created attribute name
- `sortKey($name)` - Set the table sort key using a previously created attribute name
- `globalSecondaryIndex($name)` - Create a new global secondary index
    - `partitionKey($name)` - Set the index partition key using a previously created attribute name
    - `sortKey($name)` - Set the index sort key using a previously created attribute name
- `localSecondaryIndex($name)` - Create a new local secondary index
    - `sortKey($name)` - Set the index sort key using a previously created attribute name
    - Note that the partition key may not be set manually, and is set automatically to the table partition key in
    accordance with [AWS specifications](https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-dynamodb-2012-08-10.html#createtable)

#### Example
```
public function up() {
    Schema::connection('dynamodb')->create('Account', function (Blueprint $table) {
        $table->number('id');
        $table->string('email');
        $table->binary('active');

        $table->partitionKey('id');
        $table->sortKey('email);

        $table->globalSecondaryIndex('byEmail')
            ->partitionKey('email')
            ->sortKey('active');

        $table->localSecondaryIndex('byActive')
            ->sortKey('active');
    });
}

public function down() {
    Schema::connection('dynamodb')->dropIfExists('Account');
}
```

Note that the table which is created will be given the format `ProjectName.TableName`. This is to provide some 
organization and to prevent duplicate tables between projects since DynamoDB does not support separate "databases" or 
"collections" within a single instance, as is the standard with many other database systems.
