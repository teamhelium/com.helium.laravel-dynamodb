<?php

namespace Helium\DynamoDb\Schema;

class AttributeDefinition
{
    /**
     * @var String
     */
    protected $name;

    /**
     * @var String
     */
    protected $type;


    /**
     * AttributeDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return $this
     */
    public function string()
    {
        $this->type = 'S';

        return $this;
    }

    /**
     * @return $this
     */
    public function number()
    {
        $this->type = 'N';

        return $this;
    }

    /**
     * @return $this
     */
    public function binary()
    {
        $this->type = 'B';

        return $this;
    }

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return String
     */
    public function getType()
    {
        return $this->type;
    }
}