<?php

namespace Helium\DynamoDb\Schema;

class LocalSecondaryIndexDefinition
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var String
     */
    protected $sortKey;

    /**
     * LocalSecondaryIndexDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param String $attribute
     * @return $this
     */
    public function sortKey(String $attribute)
    {
        $this->sortKey = $attribute;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return String
     */
    public function getSortKey()
    {
        return $this->sortKey;
    }
}