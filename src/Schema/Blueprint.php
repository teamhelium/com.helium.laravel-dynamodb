<?php

namespace Helium\DynamoDb\Schema;

use Aws\DynamoDb\DynamoDbClient;
use Helium\DynamoDb\Connection;

class Blueprint
{
    /**
     * @var Connection|DynamoDbClient
     */
    protected $connection;

    /**
     * @var String
     */
    protected $table;

    /**
     * @var String
     */
    protected $billingMode;

    /**
     * @var int
     */
    protected $readCapacityUnits;

    /**
     * @var int
     */
    protected $writeCapacityUnits;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var AttributeDefinition
     */
    protected $partitionKey;

    /**
     * @var AttributeDefinition
     */
    protected $sortKey;

    /**
     * @var array
     */
    protected $globalSecondaryIndexes;

    /**
     * @var array
     */
    protected $localSecondaryIndexes;


    /**
     * Blueprint constructor.
     * @param Connection $connection
     * @param $table
     */
    public function __construct(Connection $connection, $table)
    {
        $this->connection = $connection;

        $this->table = $table;
        $this->payPerRequest();

        $this->attributes = [];
        $this->globalSecondaryIndexes = [];
        $this->localSecondaryIndexes = [];
    }

    public function provisioned($readCapacityUnits = 10, $writeCapacityUnits = 10)
    {
        $this->billingMode = 'PROVISIONED';
        $this->readCapacityUnits = $readCapacityUnits;
        $this->writeCapacityUnits = $writeCapacityUnits;
    }

    public function payPerRequest()
    {
        $this->billingMode = 'PAY_PER_REQUEST';
    }

    /**
     * @param $name
     * @return AttributeDefinition
     */
    public function string($name)
    {
        return $this->addAttribute($name)->string();
    }

    /**
     * @param $name
     * @return AttributeDefinition
     */
    public function number($name)
    {
        return $this->addAttribute($name)->number();
    }

    /**
     * @param $name
     * @return AttributeDefinition
     */
    public function binary($name)
    {
        return $this->addAttribute($name)->binary();
    }

    public function partitionKey($attribute)
    {
        $this->partitionKey = $this->attributes[$attribute];

        return $this->partitionKey;
    }

    public function sortKey($attribute)
    {
        $this->sortKey = $this->attributes[$attribute];

        return $this->sortKey;
    }

    public function globalSecondaryIndex($name)
    {
        $globalSecondaryIndex = new GlobalSecondaryIndexDefinition($name);

        $this->globalSecondaryIndexes[] = $globalSecondaryIndex;

        return $globalSecondaryIndex;
    }

    public function localSecondaryIndex($name)
    {
        $localSecondaryIndex = new LocalSecondaryIndexDefinition($name);

        $this->localSecondaryIndexes[] = $localSecondaryIndex;

        return $localSecondaryIndex;
    }

    public function create()
    {
        $params = $this->buildParams();
        $this->connection->createTable($params);
    }

    public function drop()
    {
        $this->connection->deleteTable(['TableName' => $this->connection->getGroup() . '.' . $this->table]);
    }


    protected function addAttribute($name)
    {
        $attribute = new AttributeDefinition($name);

        $this->attributes[$name] = $attribute;

        return $attribute;
    }

    //TODO: Change to protected
    public function buildParams()
    {
        $params = [
            'TableName' => $this->connection->getGroup() . '.' . $this->table,
            'BillingMode' => $this->billingMode,
            'AttributeDefinitions' => [],
            'KeySchema' => [
                [
                    'AttributeName' => $this->partitionKey->getName(),
                    'KeyType' => 'HASH'
                ]
            ],
            'Tags' => [
                [
                    'Key' => 'Group',
                    'Value' => $this->connection->getGroup()
                ]
            ]
        ];

        if ($this->billingMode == 'PROVISIONED')
        {
            $params['ProvisionedThroughput'] = [
                'ReadCapacityUnits' => $this->readCapacityUnits,
                'WriteCapacityUnits' => $this->writeCapacityUnits
            ];
        }

        if (isset($this->sortKey))
        {
            $params['KeySchema'][] = [
                'AttributeName' => $this->sortKey->getName(),
                'KeyType' => 'RANGE'
            ];
        }

        /** @var AttributeDefinition $attribute */
        foreach ($this->attributes as $attribute)
        {
            $params['AttributeDefinitions'][] = [
                'AttributeName' => $attribute->getName(),
                'AttributeType' => $attribute->getType()
            ];
        }

        if (count($this->localSecondaryIndexes) > 0)
        {
            $params['LocalSecondaryIndexes'] = [];

            /** @var LocalSecondaryIndexDefinition $index */
            foreach ($this->localSecondaryIndexes as $index)
            {
                $params['LocalSecondaryIndexes'][] = [
                    'IndexName' => $index->getName(),
                    'KeySchema' => [
                        $params['KeySchema'][0],
                        [
                            'AttributeName' => $index->getSortKey(),
                            'KeyType' => 'RANGE'
                        ]
                    ],
                    'Projection' => [
                        'ProjectionType' => 'ALL'
                    ]
                ];
            }
        }

        if (count($this->globalSecondaryIndexes) > 0)
        {
            $params['GlobalSecondaryIndexes'] = [];

            /** @var GlobalSecondaryIndexDefinition $index */
            foreach ($this->globalSecondaryIndexes as $index)
            {
                $indexData = [
                    'IndexName' => $index->getName(),
                    'KeySchema' => [
                        [
                            'AttributeName' => $index->getPartitionKey(),
                            'KeyType' => 'HASH'
                        ]
                    ],
                    'Projection' => [
                        'ProjectionType' => 'ALL'
                    ]
                ];

                if ($index->getSortKey())
                {
                    $indexData['KeySchema'][] = [
                        'AttributeName' => $index->getSortKey(),
                        'KeyType' => 'RANGE'
                    ];
                }

                $params['GlobalSecondaryIndexes'][] = $indexData;
            }
        }

        return $params;
    }
}