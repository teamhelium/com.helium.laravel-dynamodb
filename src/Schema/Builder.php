<?php

namespace Helium\DynamoDb\Schema;

use Closure;
use Helium\DynamoDb\Connection;

class Builder
{
    /**
     * @inheritdoc
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $table
     * @param Closure $callback
     */
    public function table($table, Closure $callback)
    {
        $blueprint = $this->createBlueprint($table);

        if ($callback) {
            $callback($blueprint);
        }
    }

    /**
     * @param $table
     * @param Closure $callback
     * @param array $options
     */
    public function create($table, Closure $callback, array $options = [])
    {
        $blueprint = $this->createBlueprint($table);

        if ($callback) {
            $callback($blueprint);
        }

        $blueprint->create();
    }

    /**
     * @param $table
     */
    public function drop($table)
    {
        $blueprint = $this->createBlueprint($table);
        $blueprint->drop();
    }

    /**
     * @param $table
     */
    public function dropIfExists($table)
    {
        $this->drop($table);
    }

    /**
     * @param $table
     * @param Closure|null $callback
     * @return Blueprint
     */
    protected function createBlueprint($table, Closure $callback = null)
    {
        return new Blueprint($this->connection, $table);
    }
}