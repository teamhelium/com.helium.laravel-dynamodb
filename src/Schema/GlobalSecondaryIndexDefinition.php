<?php

namespace Helium\DynamoDb\Schema;

class GlobalSecondaryIndexDefinition
{
    /**
     * @var String
     */
    protected $name;

    /**
     * @var String
     */
    protected $partitionKey;

    /**
     * @var String
     */
    protected $sortKey;

    /**
     * LocalSecondaryIndexDefinition constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param String $attribute
     * @return $this
     */
    public function partitionKey($attribute)
    {
        $this->partitionKey = $attribute;

        return $this;
    }

    /**
     * @param String $attribute
     * @return $this
     */
    public function sortKey($attribute)
    {
        $this->sortKey = $attribute;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return String
     */
    public function getPartitionKey()
    {
        return $this->partitionKey;
    }

    /**
     * @return String
     */
    public function getSortKey()
    {
        return $this->sortKey;
    }
}