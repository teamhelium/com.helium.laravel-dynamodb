<?php

namespace Helium\DynamoDb;

use Aws\DynamoDb\DynamoDbClient;
use Illuminate\Database\Connection as BaseConnection;

class Connection extends BaseConnection
{
    /**
     * @var DynamoDbClient
     */
    protected $client;

    /**
     * @var String
     */
    protected $group;

    /**
     * Connection constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->group = $config['group'];

        $credentials = [
            'credentials' => $config['credentials'],
            'region' => $config['region'],
            'version' => $config['version']
        ];

        if (key_exists('endpoint', $config))
        {
            $credentials['endpoint'] = $config['endpoint'];
        }

        $this->client = new DynamoDbClient($credentials);
    }

    /**
     * @inheritdoc
     */
    public function getSchemaBuilder()
    {
        return new Schema\Builder($this);
    }

    /**
     * @inhertitDoc
     */
    public function disconnect()
    {
        unset($this->client);
    }

    /**
     * @inheritDoc
     */
    public function getDriverName()
    {
        return 'dynamodb';
    }

    /**
     * @return String
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return call_user_func_array([$this->client, $method], $arguments);
    }
}