<?php

namespace Helium\DynamoDb\Eloquent;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

trait SetsTimestamps
{
	public static function bootSetsTimestamps()
	{
		self::creating(function (Model $model) {
			$model->created_at = Carbon::now();
		});

		self::updating(function (Model $model) {
			$model->updated_at = Carbon::now();
		});
	}
}
